package com.example.dogdemo.common.mappers;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.ArrayList;
import java.util.List;

public interface TwoWayMapper<From, To> extends Mapper<From, To> {

    @Nullable
    From reverseMap(@Nullable To value);

    @NonNull
    default List<From> reverseMap(@Nullable List<To> values) {
        if (values == null) return new ArrayList<>(0);

        List<From> returnValues = new ArrayList<>(values.size());
        for (To value : values) {
            returnValues.add(reverseMap(value));
        }
        return returnValues;
    }

}
