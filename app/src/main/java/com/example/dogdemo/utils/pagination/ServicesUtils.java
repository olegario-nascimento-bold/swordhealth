package com.example.dogdemo.utils.pagination;


import android.app.ActivityManager;
import android.content.Context;

import javax.inject.Inject;

public class ServicesUtils {

    Context mContext;

    @Inject
    public ServicesUtils(Context context) {
        mContext = context;
    }

    public boolean isServiceAlive(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) mContext.getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }
}