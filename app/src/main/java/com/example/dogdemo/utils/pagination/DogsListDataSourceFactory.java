package com.example.dogdemo.utils.pagination;

import androidx.paging.DataSource;

import com.example.dogdemo.domain.managers.DogsManager;
import com.example.dogdemo.presentation.models.DogPresentation;
import com.example.dogdemo.presentation.utils.pagination.datasource.DogsListDataSource;
import com.example.dogdemo.presentation.utils.pagination.datasource.base.OnDataSourceLoading;

import javax.inject.Inject;

public class DogsListDataSourceFactory extends DataSource.Factory<Integer, DogPresentation> {

    private final DogsManager dogsManager;
    private final OnDataSourceLoading onDataSourceLoading;

    @Inject
    public DogsListDataSourceFactory(DogsManager dogsManager, OnDataSourceLoading loading) {
        this.dogsManager = dogsManager;
        this.onDataSourceLoading = loading;
    }

    @Override
    public DataSource<Integer, DogPresentation> create() {
        DogsListDataSource source = new DogsListDataSource(dogsManager);
        source.setOnDataSourceLoading(onDataSourceLoading);
        return source;
    }
}