package com.example.dogdemo.utils;


import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import androidx.annotation.AnimRes;
import androidx.annotation.ArrayRes;
import androidx.annotation.BoolRes;
import androidx.annotation.ColorInt;
import androidx.annotation.ColorRes;
import androidx.annotation.DimenRes;
import androidx.annotation.DrawableRes;
import androidx.annotation.FontRes;
import androidx.annotation.IntegerRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.PluralsRes;
import androidx.annotation.RawRes;
import androidx.annotation.StringRes;
import androidx.core.content.ContextCompat;
import androidx.core.content.res.ResourcesCompat;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import javax.inject.Inject;

public class ResourceProvider {

    private final Context mContext;

    @Inject
    public ResourceProvider(Context context) {
        mContext = context;
    }

    @NonNull
    public CharSequence getText(@StringRes int resId) {
        return mContext.getText(resId);
    }

    @NonNull
    public CharSequence[] getTextArray(@ArrayRes int resId) {
        return mContext.getResources().getTextArray(resId);
    }

    @NonNull
    public CharSequence getQuantityText(@PluralsRes int resId, int quantity) {
        return mContext.getResources().getQuantityText(resId, quantity);
    }

    @NonNull
    public String getString(@StringRes int resId) {
        return mContext.getString(resId);
    }

    @NonNull
    public String getString(@StringRes int resId, Object... formatArgs) {
        return mContext.getString(resId, formatArgs);
    }

    @NonNull
    public String[] getStringArray(@ArrayRes int resId) {
        return mContext.getResources().getStringArray(resId);
    }

    @NonNull
    public String getQuantityString(@PluralsRes int resId, int quantity) {
        return mContext.getResources().getQuantityString(resId, quantity);
    }

    @NonNull
    public String getQuantityString(@PluralsRes int resId, int quantity, Object... formatArgs) {
        return mContext.getResources().getQuantityString(resId, quantity, formatArgs);
    }

    public int getInteger(@IntegerRes int resId) {
        return mContext.getResources().getInteger(resId);
    }

    @NonNull
    public int[] getIntArray(@ArrayRes int resId) {
        return mContext.getResources().getIntArray(resId);
    }

    public boolean getBoolean(@BoolRes int resId) {
        return mContext.getResources().getBoolean(resId);
    }

    public float getDimension(@DimenRes int resId) {
        return mContext.getResources().getDimension(resId);
    }

    public int getDimensionPixelSize(@DimenRes int resId) {
        return mContext.getResources().getDimensionPixelSize(resId);
    }

    public int getDimensionPixelOffset(@DimenRes int resId) {
        return mContext.getResources().getDimensionPixelOffset(resId);
    }

    @Nullable
    public Drawable getDrawable(@DrawableRes int resId) {
        return ContextCompat.getDrawable(mContext, resId);
    }

    @ColorInt
    public int getColor(@ColorRes int resId) {
        return ContextCompat.getColor(mContext, resId);
    }

    @Nullable
    public ColorStateList getColorStateList(@ColorRes int resId) {
        return ContextCompat.getColorStateList(mContext, resId);
    }

    @Nullable
    public Typeface getFont(@FontRes int id) throws Resources.NotFoundException {
        return ResourcesCompat.getFont(mContext, id);
    }

    public Animation loadAnimation(@AnimRes int id) {
        return AnimationUtils.loadAnimation(mContext, id);
    }

    public String getRawJson(@RawRes int id) throws Resources.NotFoundException, IOException {
        BufferedReader r = new BufferedReader(new InputStreamReader(mContext.getResources().openRawResource(id)));
        StringBuilder total = new StringBuilder();
        String line;
        while ((line = r.readLine()) != null) {
            total.append(line).append('\n');
        }
        return total.toString();
    }
}