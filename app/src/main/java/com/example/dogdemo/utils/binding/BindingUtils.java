package com.example.dogdemo.utils.binding;


import android.graphics.drawable.Drawable;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.ImageView;

import androidx.databinding.BindingAdapter;
import androidx.databinding.InverseBindingAdapter;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import com.example.dogdemo.presentation.GlideApp;

public class BindingUtils {
    @BindingAdapter("android:visibility")
    public static void setVisibility(View view, Boolean value) {
        view.setVisibility(value ? View.VISIBLE : View.GONE);
    }

    @InverseBindingAdapter(attribute = "android:checked")
    public static boolean get(CompoundButton compoundButton) {
        return compoundButton.isChecked();
    }

    @BindingAdapter("pageMargin")
    public static void setPageMargin(ViewPager v, float pageMargin) {
        v.setPageMargin((int) pageMargin);
    }

    @BindingAdapter("nestedScrollingEnabled")
    public static void setNestedScrollingEnabled(RecyclerView v, boolean enabled) {
        v.setNestedScrollingEnabled(enabled);
    }

    @BindingAdapter(value = {"imageUrl", "placeholderResIdUrl", "placeholderDrawableUrl"}, requireAll = false)
    public static void setImageUrl(ImageView imageView, String url, int placeholderResIdUrl, Drawable placeholderDrawableUrl) {
        //url = Utils.normalizeUrl(url);
        if (placeholderResIdUrl > 0) {

            GlideApp.with(imageView.getContext())
                    .load(url)
                    .placeholder(placeholderResIdUrl)
                    .into(imageView);
        } else {
            GlideApp.with(imageView.getContext())
                    .load(url)
                    .placeholder(placeholderDrawableUrl)
                    .into(imageView);
        }
    }
}
