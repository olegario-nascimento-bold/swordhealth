package com.example.dogdemo.domain.managers;

import com.example.dogdemo.data.api.services.DogsService;
import com.example.dogdemo.domain.mappers.DogMapper;
import com.example.dogdemo.domain.models.Dog;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Single;

public class DogsManager {
    private final DogsService service;
    private DogMapper dogsMapper;

    @Inject
    DogsManager(DogsService service, DogMapper dogsMapper) {
        this.service = service;
        this.dogsMapper = dogsMapper;
    }

    public Single<List<Dog>> getDogs() {
        return service.getDogsBreed()
                .map(list -> dogsMapper.map(list));
    }

    public Single<List<Dog>> getDogs(String breed, Integer page, Integer limit) {
        return service.getDogsBreed(breed, page, limit)
                .map(list -> dogsMapper.map(list));
    }

    public Single<List<Dog>> searchDogs(String query) {
        return service.searchDogs(query)
                .map(list -> dogsMapper.map(list));
    }
}
