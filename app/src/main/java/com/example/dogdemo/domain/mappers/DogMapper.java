package com.example.dogdemo.domain.mappers;

import androidx.annotation.Nullable;

import com.example.dogdemo.common.mappers.Mapper;
import com.example.dogdemo.data.api.models.response.DogRemote;
import com.example.dogdemo.domain.models.Dog;

import javax.inject.Inject;

public class DogMapper implements Mapper<DogRemote, Dog> {

    @Inject
    DogMapper() {
    }

    @Nullable
    @Override
    public Dog map(@Nullable DogRemote value) {
        if (value == null) {
            return null;
        }
        Dog dog = new Dog();
        dog.setId(value.getId());
        dog.setBredFor(value.getBredFor());
        dog.setBreedGroup(value.getBreedGroup());
        dog.setHeight(value.getHeight());
        dog.setLifeSpan(value.getLifeSpan());
        dog.setName(value.getName());
        dog.setOrigin(value.getOrigin());
        dog.setTemperament(value.getTemperament());
        dog.setWeight(value.getWeight());
        dog.setImage(value.getImage());
        return dog;
    }
}