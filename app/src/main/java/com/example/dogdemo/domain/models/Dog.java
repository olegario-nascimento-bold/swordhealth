package com.example.dogdemo.domain.models;

import com.example.dogdemo.data.api.models.response.ImageRemote;
import com.example.dogdemo.data.api.models.response.MeasuresRemote;

public class Dog {

    private String id;
    private String bredFor;
    private String breedGroup;
    private MeasuresRemote height;
    private String lifeSpan;
    private String name;
    private String origin;
    private String temperament;
    private MeasuresRemote weight;
    private ImageRemote image;


    public Dog() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getBredFor() {
        return bredFor;
    }

    public void setBredFor(String bredFor) {
        this.bredFor = bredFor;
    }

    public String getBreedGroup() {
        return breedGroup;
    }

    public void setBreedGroup(String breedGroup) {
        this.breedGroup = breedGroup;
    }

    public MeasuresRemote getHeight() {
        return height;
    }

    public void setHeight(MeasuresRemote height) {
        this.height = height;
    }

    public String getLifeSpan() {
        return lifeSpan;
    }

    public void setLifeSpan(String lifeSpan) {
        this.lifeSpan = lifeSpan;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public String getTemperament() {
        return temperament;
    }

    public void setTemperament(String temperament) {
        this.temperament = temperament;
    }

    public MeasuresRemote getWeight() {
        return weight;
    }

    public void setWeight(MeasuresRemote weight) {
        this.weight = weight;
    }

    public ImageRemote getImage() {
        return image;
    }

    public void setImage(ImageRemote image) {
        this.image = image;
    }
}
