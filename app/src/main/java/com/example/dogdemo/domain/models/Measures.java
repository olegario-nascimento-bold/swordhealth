package com.example.dogdemo.domain.models;

public class Measures {

    private String imperial;
    private String metric;

    public Measures() {
    }

    public String getImperial() {
        return imperial;
    }

    public void setImperial(String imperial) {
        this.imperial = imperial;
    }

    public String getMetric() {
        return metric;
    }

    public void setMetric(String metric) {
        this.metric = metric;
    }
}
