package com.example.dogdemo.data.api.exceptions;


import com.google.gson.annotations.SerializedName;

public enum ApiExceptionsEnum {
    @SerializedName("500000")
    UNKNOWN_ERROR(500000);

    int id;

    ApiExceptionsEnum(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }
}
