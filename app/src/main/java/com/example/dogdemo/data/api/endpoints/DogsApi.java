package com.example.dogdemo.data.api.endpoints;

import com.example.dogdemo.data.api.models.response.DogRemote;

import java.util.List;

import io.reactivex.Single;
import retrofit2.Response;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface DogsApi {

    String PREFIX = "breeds/";

    @GET(PREFIX)
    Single<Response<List<DogRemote>>> getBreeds(
            @Query("attach_breed") String breed,
            @Query("page") Integer page,
            @Query("limit") Integer limit
    );

    @GET(PREFIX + "search")
    Single<Response<List<DogRemote>>> searchBreeds(
            @Query("q") String q
    );
}