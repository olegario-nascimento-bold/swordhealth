package com.example.dogdemo.data.api.adapters;

import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;

import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;

import timber.log.Timber;

public class DateGsonAdapter extends TypeAdapter<LocalDate> {
    @Override
    public void write(JsonWriter out, LocalDate value) throws IOException {
        out.value(value.format(getFormatter()));
    }

    @Override
    public LocalDate read(JsonReader in) throws IOException {
        try {
            return LocalDate.parse(in.nextString(), getFormatter());
        } catch (DateTimeParseException | IllegalStateException e) {
            Timber.e(e);
            return LocalDate.now();
        }
    }

    private DateTimeFormatter getFormatter() {
        return DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss");
    }
}