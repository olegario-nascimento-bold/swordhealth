package com.example.dogdemo.data.api.exceptions.api;

import com.example.dogdemo.data.api.exceptions.base.BaseException;
import com.example.dogdemo.data.api.services.base.ApiError;

public class UnknownErrorException extends BaseException {
    public UnknownErrorException(ApiError apiError) {
        super(apiError);
    }

    public UnknownErrorException(Throwable throwable, ApiError apiError) {
        super(throwable, apiError);
    }
}
