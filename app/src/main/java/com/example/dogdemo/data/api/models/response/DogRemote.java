package com.example.dogdemo.data.api.models.response;

import com.google.gson.annotations.SerializedName;

public class DogRemote {
    @SerializedName("id")
    private String id;
    @SerializedName("bred_for")
    private String bredFor;
    @SerializedName("breed_group")
    private String breedGroup;
    @SerializedName("height")
    private MeasuresRemote height;
    @SerializedName("life_span")
    private String lifeSpan;
    @SerializedName("name")
    private String name;
    @SerializedName("origin")
    private String origin;
    @SerializedName("temperament")
    private String temperament;
    @SerializedName("weight")
    private MeasuresRemote weight;
    @SerializedName("image")
    private ImageRemote image;


    public DogRemote() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getBredFor() {
        return bredFor;
    }

    public void setBredFor(String bredFor) {
        this.bredFor = bredFor;
    }

    public String getBreedGroup() {
        return breedGroup;
    }

    public void setBreedGroup(String breedGroup) {
        this.breedGroup = breedGroup;
    }

    public MeasuresRemote getHeight() {
        return height;
    }

    public void setHeight(MeasuresRemote height) {
        this.height = height;
    }

    public String getLifeSpan() {
        return lifeSpan;
    }

    public void setLifeSpan(String lifeSpan) {
        this.lifeSpan = lifeSpan;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public String getTemperament() {
        return temperament;
    }

    public void setTemperament(String temperament) {
        this.temperament = temperament;
    }

    public MeasuresRemote getWeight() {
        return weight;
    }

    public void setWeight(MeasuresRemote weight) {
        this.weight = weight;
    }

    public ImageRemote getImage() {
        return image;
    }

    public void setHeight(ImageRemote image) {
        this.image = image;
    }


}
