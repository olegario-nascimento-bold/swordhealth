package com.example.dogdemo.data.api.services.base;

import android.accounts.NetworkErrorException;

import androidx.annotation.StringRes;

import com.example.dogdemo.R;
import com.example.dogdemo.data.api.exceptions.ApiExceptionsEnum;
import com.example.dogdemo.data.api.exceptions.api.UnknownErrorException;
import com.example.dogdemo.data.api.exceptions.base.BaseException;
import com.example.dogdemo.data.api.exceptions.http.GenericErrorException;
import com.example.dogdemo.data.api.exceptions.http.InternetConnectionException;
import com.example.dogdemo.data.api.exceptions.http.NotFoundException;
import com.example.dogdemo.data.api.exceptions.http.ServerException;
import com.example.dogdemo.data.api.exceptions.http.UnauthorizedException;
import com.example.dogdemo.data.api.models.error.ServerErrorObject;
import com.example.dogdemo.utils.ResourceProvider;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import io.reactivex.Single;
import io.reactivex.SingleTransformer;
import io.reactivex.annotations.NonNull;

import io.reactivex.exceptions.UndeliverableException;
import retrofit2.HttpException;
import retrofit2.Response;
import retrofit2.Retrofit;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;


public abstract class ApiService {

    private final Gson gson;
    protected Retrofit retrofit;
    protected ResourceProvider resourceProvider;

    public ApiService(Retrofit retrofit, ResourceProvider resourceProvider, Gson gson) {
        this.retrofit = retrofit;
        this.resourceProvider = resourceProvider;
        this.gson = gson;
    }

    protected <T> SingleTransformer<Response<T>, Response<T>> networkTransform() {
        return upstream -> upstream
                .onErrorResumeNext(throwable -> Single.error(mapThrowable(throwable)))
                .flatMap(response -> {
                    if (!response.isSuccessful()) {
                        return Single.error(mapApiThrowable(getApiError(response)));
                    }
                    return Single.just(response);
                });
    }

    protected <T> SingleTransformer<Response<T>, T> networkMapTransform() {
        return upstream -> upstream
                .compose(networkTransform())
                .map(Response::body);
    }

    protected <T> SingleTransformer<Response<T>, Response<T>> networkVoidTransform() {
        return upstream -> upstream
                .compose(networkTransform());
    }

    @NonNull
    private Throwable mapThrowable(Throwable throwable) {
        if (throwable instanceof HttpException) {
            HttpException httpException = (HttpException) throwable;
            Response response = httpException.response();
            return mapHttpThrowable(throwable, response.code(), response.message());

        } else if (throwable instanceof NetworkErrorException) {
            return new NetworkErrorException(throwable);

        } else if (throwable instanceof SocketTimeoutException || throwable instanceof UnknownHostException) {
            return new SocketTimeoutException(resourceProvider.getString(R.string.err_timeout));

        } else if (throwable instanceof FileNotFoundException) {
            return new FileNotFoundException(throwable.getMessage());
        } else if (throwable instanceof UndeliverableException) {
            return new UndeliverableException(throwable.getCause());
        } else {
            return new InternetConnectionException(throwable, createApiError(R.string.err_api_generic));
        }
    }

    @NonNull
    private BaseException mapHttpThrowable(Throwable throwable, int code, String message) {
        BaseException exception;
        switch (code) {
            case 401:
                exception = new UnauthorizedException(throwable, createApiError(message));
                break;
            case 404:
                exception = new NotFoundException(throwable, createApiError(message));
                break;
            case 500:
            case 501:
            case 502:
            case 503:
            case 504:
                exception = new ServerException(throwable, createApiError(message));
                break;
            default:
                exception = new GenericErrorException(throwable, createApiError(message));
        }
        return exception;
    }

    @NonNull
    private BaseException mapApiThrowable(ServerErrorObject code) {
        BaseException exception;
        if (code.getError() == null) {
            exception = new UnknownErrorException(createApiError(code.getMessage()));
        } else {
            switch (code.getError()) {
                case UNKNOWN_ERROR:
                default:
                    exception = new UnknownErrorException(createApiError(code.getMessage()));
                    break;
            }
        }

        return exception;
    }

    @NonNull
    private ApiError createApiError(@StringRes int resId) {
        return new ApiError(resourceProvider.getString(resId));
    }

    @NonNull
    private ApiError createApiError(@NonNull String message) {
        return new ApiError(message);
    }

    private ServerErrorObject getApiError(Response response) {
        try {
            return getServerError(response);
        } catch (IOException | JsonSyntaxException exception) {
            return new ServerErrorObject(exception.getMessage(), ApiExceptionsEnum.UNKNOWN_ERROR);
        }
    }

    private ServerErrorObject getServerError(Response response) throws JsonSyntaxException, IOException {
        return response.errorBody() == null ? new ServerErrorObject("", ApiExceptionsEnum.UNKNOWN_ERROR) : gson.fromJson(response.errorBody().string(), ServerErrorObject.class);
    }
}
