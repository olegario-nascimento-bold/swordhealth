package com.example.dogdemo.data.api.models.response;

import com.google.gson.annotations.SerializedName;

public class MeasuresRemote {

    @SerializedName("imperial")
    private String imperial;
    @SerializedName("metric")
    private String metric;

    public MeasuresRemote() {
    }

    public String getImperial() {
        return imperial;
    }

    public void setImperial(String imperial) {
        this.imperial = imperial;
    }

    public String getMetric() {
        return metric;
    }

    public void setMetric(String metric) {
        this.metric = metric;
    }
}
