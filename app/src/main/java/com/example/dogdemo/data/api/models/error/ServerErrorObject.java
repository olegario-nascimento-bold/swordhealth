package com.example.dogdemo.data.api.models.error;

import com.example.dogdemo.data.api.exceptions.ApiExceptionsEnum;

public class ServerErrorObject {
    private String message;
    private ApiExceptionsEnum error;

    public ServerErrorObject(String message, ApiExceptionsEnum error) {
        this.message = message;
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public ApiExceptionsEnum getError() {
        return error;
    }
}
