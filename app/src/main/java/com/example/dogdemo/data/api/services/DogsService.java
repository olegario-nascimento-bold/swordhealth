package com.example.dogdemo.data.api.services;

import com.example.dogdemo.data.api.endpoints.DogsApi;
import com.example.dogdemo.data.api.models.response.DogRemote;
import com.example.dogdemo.data.api.services.base.ApiService;
import com.example.dogdemo.utils.ResourceProvider;
import com.google.gson.Gson;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Single;
import retrofit2.Retrofit;

public class DogsService extends ApiService {

    private DogsApi api;

    @Inject
    public DogsService(Retrofit retrofit, ResourceProvider resourceProvider, Gson gson) {
        super(retrofit, resourceProvider, gson);
        this.api = retrofit.create(DogsApi.class);
    }

    public Single<List<DogRemote>> getDogsBreed() {
        return api.getBreeds("", null, null)
                .compose(networkMapTransform());
    }

    public Single<List<DogRemote>> getDogsBreed(String breed) {
        return api.getBreeds(breed, null, null)
                .compose(networkMapTransform());
    }

    public Single<List<DogRemote>> getDogsBreed(String breed, Integer page) {
        return api.getBreeds(breed, page, null)
                .compose(networkMapTransform());
    }

    public Single<List<DogRemote>> getDogsBreed(String breed, Integer page, Integer limit ) {
        return api.getBreeds(breed, page, limit)
                .compose(networkMapTransform());
    }

    public Single<List<DogRemote>> searchDogs(String query) {
        return api.searchBreeds(query)
                .compose(networkMapTransform());
    }
}
