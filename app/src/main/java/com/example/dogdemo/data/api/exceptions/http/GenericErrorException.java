package com.example.dogdemo.data.api.exceptions.http;

import com.example.dogdemo.data.api.exceptions.base.BaseException;
import com.example.dogdemo.data.api.services.base.ApiError;

public class GenericErrorException extends BaseException {

    public GenericErrorException(ApiError apiError) {
        super(apiError);
    }

    public GenericErrorException(Throwable throwable, ApiError apiError) {
        super(throwable, apiError);
    }
}
