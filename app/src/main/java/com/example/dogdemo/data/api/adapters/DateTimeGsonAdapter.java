package com.example.dogdemo.data.api.adapters;

import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;

public class DateTimeGsonAdapter extends TypeAdapter<LocalDateTime> {
    @Override
    public void write(JsonWriter out, LocalDateTime value) throws IOException {
        out.value(value.format(getFormatter()));
    }

    @Override
    public LocalDateTime read(JsonReader in) throws IOException {
        try {
            return LocalDateTime.parse(in.nextString(), getFormatter());
        } catch (DateTimeParseException exception) {
            exception.printStackTrace();
            return LocalDateTime.now();
        } catch (IllegalStateException ise) {
            ise.printStackTrace();
            return LocalDateTime.now();
        }
    }

    private DateTimeFormatter getFormatter() {
        return DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss");
    }
}