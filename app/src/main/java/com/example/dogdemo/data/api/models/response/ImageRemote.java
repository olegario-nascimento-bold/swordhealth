package com.example.dogdemo.data.api.models.response;

import com.google.gson.annotations.SerializedName;

public class ImageRemote {

    @SerializedName("id")
    private String id;
    @SerializedName("height")
    private String height;
    @SerializedName("url")
    private String url;
    @SerializedName("width")
    private String width;

    public ImageRemote() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getHeight() {
        return height;
    }

    public void setHeight(String height) {
        this.height = height;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getWidth() {
        return width;
    }

    public void setWidth(String width) {
        this.width = width;
    }

}
