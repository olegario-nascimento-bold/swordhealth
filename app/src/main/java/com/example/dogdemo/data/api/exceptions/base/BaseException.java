package com.example.dogdemo.data.api.exceptions.base;

import com.example.dogdemo.data.api.services.base.ApiError;

public class BaseException extends Throwable {

    private ApiError mApiError;

    public BaseException() {}

    public BaseException(ApiError apiError) {
        super();
        mApiError = apiError;
    }

    public BaseException(Throwable throwable, ApiError apiError) {
        super(throwable);
        mApiError = apiError;
    }

    public ApiError getApiError() {
        return mApiError;
    }

    @Override
    public String getMessage() {
        return mApiError != null ? mApiError.getMessage() : super.getMessage();
    }
}