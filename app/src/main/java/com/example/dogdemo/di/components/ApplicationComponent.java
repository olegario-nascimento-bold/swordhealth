package com.example.dogdemo.di.components;

import com.example.dogdemo.DogApp;
import com.example.dogdemo.di.modules.ApplicationModule;
import com.example.dogdemo.di.modules.NetworkModule;
import com.example.dogdemo.di.modules.activities.binding.ActivityBindingModule;

import javax.inject.Singleton;

import dagger.Component;
import dagger.android.AndroidInjector;
import dagger.android.support.AndroidSupportInjectionModule;

@Singleton
@Component(modules = {
        ApplicationModule.class,
        NetworkModule.class,
        ActivityBindingModule.class,
        AndroidSupportInjectionModule.class
})
public interface ApplicationComponent extends AndroidInjector<DogApp> {

    @Component.Builder
    abstract class Builder extends AndroidInjector.Builder<DogApp> {
    }
}