package com.example.dogdemo.di.modules.activities;

import com.example.dogdemo.presentation.models.DogPresentation;
import com.example.dogdemo.presentation.ui.main.home.detail.DogDetailActivity;

import dagger.Module;
import dagger.Provides;

@Module
public abstract class DogDetailActivityModule extends BaseActivityModule<DogDetailActivity> {
    @Provides
    static DogPresentation provideEvent(DogDetailActivity activity) {
        return activity.getDog();
    }
}