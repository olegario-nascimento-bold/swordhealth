package com.example.dogdemo.di.modules.fragments;

import com.example.dogdemo.presentation.ui.main.home.HomeFragment;

import dagger.Module;

@Module
public abstract class HomeFragmentModule extends BaseFragmentModule<HomeFragment> {
}
