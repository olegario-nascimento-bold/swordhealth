package com.example.dogdemo.di.modules.activities;

import com.example.dogdemo.presentation.ui.splash.SplashScreenActivity;

import dagger.Module;

@Module
public abstract class SplashActivityModule extends BaseActivityModule<SplashScreenActivity> {
}