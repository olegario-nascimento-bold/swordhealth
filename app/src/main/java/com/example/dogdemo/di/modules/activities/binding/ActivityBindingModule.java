package com.example.dogdemo.di.modules.activities.binding;


import com.example.dogdemo.di.modules.activities.DogDetailActivityModule;
import com.example.dogdemo.di.modules.activities.MainActivityModule;
import com.example.dogdemo.di.modules.activities.SplashActivityModule;
import com.example.dogdemo.di.scopes.ActivityScope;
import com.example.dogdemo.presentation.ui.main.MainActivity;
import com.example.dogdemo.presentation.ui.main.home.detail.DogDetailActivity;
import com.example.dogdemo.presentation.ui.splash.SplashScreenActivity;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class ActivityBindingModule {

    @ActivityScope
    @ContributesAndroidInjector(modules = MainActivityModule.class)
    abstract MainActivity mMainActivity();

    @ActivityScope
    @ContributesAndroidInjector(modules = SplashActivityModule.class)
    abstract SplashScreenActivity mSplashScreenActivity();

    @ActivityScope
    @ContributesAndroidInjector(modules = DogDetailActivityModule.class)
    abstract DogDetailActivity mDogDetailActivity();


}