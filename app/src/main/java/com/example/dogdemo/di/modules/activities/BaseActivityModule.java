package com.example.dogdemo.di.modules.activities;

import android.content.Context;

import androidx.appcompat.app.AppCompatActivity;

import com.example.dogdemo.di.qualifiers.ActivityContext;

import dagger.Binds;
import dagger.Module;
import dagger.Provides;

@Module
abstract class BaseActivityModule<A extends AppCompatActivity> {

    @Binds
    abstract AppCompatActivity activity(A activity);

    @Provides
    @ActivityContext
    static Context provideContext(AppCompatActivity activity) {
        return activity;
    }
}