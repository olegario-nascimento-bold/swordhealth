package com.example.dogdemo.di.modules;


import android.content.Context;

import com.example.dogdemo.BuildConfig;
import com.example.dogdemo.common.schedulers.SchedulerProvider;
import com.example.dogdemo.data.api.adapters.DateGsonAdapter;
import com.example.dogdemo.data.api.adapters.DateTimeGsonAdapter;
import com.example.dogdemo.data.api.adapters.LocalTimeGsonAdapter;
import com.example.dogdemo.di.qualifiers.BaseApiUrl;
import com.franmontiel.persistentcookiejar.ClearableCookieJar;
import com.franmontiel.persistentcookiejar.PersistentCookieJar;
import com.franmontiel.persistentcookiejar.cache.SetCookieCache;
import com.franmontiel.persistentcookiejar.persistence.SharedPrefsCookiePersistor;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.example.dogdemo.Constants.CONNECT_TIMEOUT;
import static com.example.dogdemo.Constants.READ_TIMEOUT;
import static com.example.dogdemo.Constants.WRITE_TIMEOUT;


@Module
public class NetworkModule {

    @Provides
    @BaseApiUrl
    String provideBaseApiUrl() {
        return BuildConfig.BASE_API_URL;
    }

    @Provides
    HttpLoggingInterceptor provideLoggingInterceptor() {
        return new HttpLoggingInterceptor()
                .setLevel(BuildConfig.DEBUG ? HttpLoggingInterceptor.Level.BODY : HttpLoggingInterceptor.Level.BASIC);
    }

    @Provides
    Interceptor provideHeaderInterceptor() {
        return chain -> {
            Request request = chain.request().newBuilder()
                    .addHeader("Accept", "application/json")
                    // TODO: DEBUG
                    .addHeader("Language", Locale.getDefault().toString().replace("_", "-"))
                    //.addHeader("Language", "pt")
                    .build();
            return chain.proceed(request);
        };
    }

    @Provides
    @Singleton
    ClearableCookieJar provideCookieJar(Context context) {
        return new PersistentCookieJar(new SetCookieCache(), new SharedPrefsCookiePersistor(context));
    }

    @Provides
    @Singleton
    OkHttpClient provideHttpClient(HttpLoggingInterceptor loggingInterceptor, Interceptor headerInterceptor, ClearableCookieJar cookieJar) {
        return new OkHttpClient.Builder()
                .cookieJar(cookieJar)
                .connectTimeout(CONNECT_TIMEOUT, TimeUnit.SECONDS)
                .readTimeout(READ_TIMEOUT, TimeUnit.SECONDS)
                .writeTimeout(WRITE_TIMEOUT, TimeUnit.SECONDS)
                .addInterceptor(headerInterceptor)
                .addInterceptor(loggingInterceptor)
                .build();
    }

    @Provides
    @Singleton
    Retrofit provideRetrofit(OkHttpClient client, @BaseApiUrl String baseApiUrl, SchedulerProvider schedulerProvider, Gson gson) {
        return new Retrofit.Builder()
                .baseUrl(baseApiUrl)
                .client(client)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.createWithScheduler(schedulerProvider.io()))
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();
    }

    @Provides
    Gson provideGson() {
        GsonBuilder builder = new GsonBuilder();
        builder.registerTypeAdapter(LocalDate.class, new DateGsonAdapter().nullSafe());
        builder.registerTypeAdapter(LocalDateTime.class, new DateTimeGsonAdapter().nullSafe());
        builder.registerTypeAdapter(LocalTime.class, new LocalTimeGsonAdapter().nullSafe());
        builder.serializeNulls();
        return builder.create();
    }
}
