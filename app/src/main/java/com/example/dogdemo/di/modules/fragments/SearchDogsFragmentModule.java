package com.example.dogdemo.di.modules.fragments;

import com.example.dogdemo.presentation.ui.main.search.SearchDogsFragment;

import dagger.Module;

@Module
public abstract class SearchDogsFragmentModule extends BaseFragmentModule<SearchDogsFragment> {
}
