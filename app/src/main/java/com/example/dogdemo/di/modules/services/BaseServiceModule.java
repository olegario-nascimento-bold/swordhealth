package com.example.dogdemo.di.modules.services;

import android.app.Service;
import android.content.Context;

import com.example.dogdemo.di.qualifiers.ServiceContext;

import dagger.Binds;
import dagger.Module;
import dagger.Provides;

@Module
abstract class BaseServiceModule<S extends Service> {

    @Binds
    abstract Service service(S service);

    @Provides
    @ServiceContext
    static Context provideContext(Service service) {
        return service;
    }
}