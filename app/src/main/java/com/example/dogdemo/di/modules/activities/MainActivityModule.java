package com.example.dogdemo.di.modules.activities;


import com.example.dogdemo.di.modules.fragments.HomeFragmentModule;
import com.example.dogdemo.di.modules.fragments.SearchDogsFragmentModule;
import com.example.dogdemo.di.scopes.FragmentScope;
import com.example.dogdemo.presentation.ui.main.MainActivity;
import com.example.dogdemo.presentation.ui.main.home.HomeFragment;
import com.example.dogdemo.presentation.ui.main.search.SearchDogsFragment;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class MainActivityModule extends BaseActivityModule<MainActivity> {
    @FragmentScope
    @ContributesAndroidInjector(modules = HomeFragmentModule.class)
    abstract HomeFragment homeFragmentSubInjector();

    @FragmentScope
    @ContributesAndroidInjector(modules = SearchDogsFragmentModule.class)
    abstract SearchDogsFragment searchDogsFragmentSubInjector();
}