package com.example.dogdemo.di.modules;


import android.app.Application;
import android.content.Context;
import android.location.LocationManager;

import com.example.dogdemo.DogApp;
import com.example.dogdemo.common.schedulers.AppSchedulerProvider;
import com.example.dogdemo.common.schedulers.SchedulerProvider;

import javax.inject.Singleton;

import dagger.Binds;
import dagger.Module;
import dagger.Provides;

@Module
public abstract class ApplicationModule {

    @Binds
    abstract Application application(DogApp app);

    @Provides
    static Context provideContext(Application application) {
        return application;
    }

    @Provides
    static SchedulerProvider provideSchedulerProvider() {
        return new AppSchedulerProvider();
    }
}