package com.example.dogdemo.presentation.base.loading;

import androidx.databinding.ViewDataBinding;

import com.example.dogdemo.presentation.base.toolbar.BaseToolbarFragment;
import com.example.dogdemo.presentation.views.LoadingView;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.schedulers.Schedulers;

public abstract class BaseLoadingToolbarFragment<ViewBinding extends ViewDataBinding, ViewModel extends BaseLoadingToolbarViewModel>
        extends BaseToolbarFragment<ViewBinding, ViewModel> {

    @NonNull
    protected abstract LoadingView getLoadingViewBinding();

    @Override
    protected void registerObservables() {
        super.registerObservables();
        getViewModel().observeLoading()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(this::addDisposable)
                .subscribe(isLoading -> getLoadingViewBinding().setShow(isLoading));
    }
}
