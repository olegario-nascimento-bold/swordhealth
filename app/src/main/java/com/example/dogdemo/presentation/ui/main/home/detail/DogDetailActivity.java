package com.example.dogdemo.presentation.ui.main.home.detail;

import androidx.annotation.Nullable;
import androidx.viewpager.widget.ViewPager;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.example.dogdemo.R;
import com.example.dogdemo.databinding.ActivityDogDetailBinding;
import com.example.dogdemo.presentation.base.toolbar.BaseToolbarActivity;
import com.example.dogdemo.presentation.models.DogPresentation;
import com.example.dogdemo.presentation.utils.ActivityTransition;
import com.example.dogdemo.presentation.utils.transitions.VerticalSlideTransition;

public class DogDetailActivity extends BaseToolbarActivity<ActivityDogDetailBinding, DogDetailViewModel>
        implements ViewPager.OnPageChangeListener {
    private static final String EXTRA_DOG = "extra_dog";

    public static void launch(Context context, DogPresentation dog) {
        Intent intent = new Intent(context, DogDetailActivity.class);
        intent.putExtra(EXTRA_DOG, dog);
        context.startActivity(intent);
    }

    private DogPresentation dog;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected ActivityTransition getActivityTransition() {
        return new VerticalSlideTransition();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public void finish() {
        super.finish();
    }

    @Override
    public int layoutResId() {
        return R.layout.activity_dog_detail;
    }

    @Override
    protected void initViews(ActivityDogDetailBinding binding) {
        initToolbar(binding.actionbar.toolbar);
    }

    @Override
    public Class<DogDetailViewModel> viewModelClass() {
        return DogDetailViewModel.class;
    }

    @Override
    protected void initArguments(Bundle args) {
        dog = args.getParcelable(EXTRA_DOG);
    }

    public DogPresentation getDog() {
        return dog;
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {

    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }
}