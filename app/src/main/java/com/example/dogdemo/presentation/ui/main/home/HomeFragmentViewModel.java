package com.example.dogdemo.presentation.ui.main.home;

import android.annotation.SuppressLint;
import android.graphics.drawable.Drawable;
import android.view.View;

import androidx.databinding.Bindable;
import androidx.databinding.ObservableArrayList;
import androidx.databinding.ObservableList;
import androidx.databinding.library.baseAdapters.BR;

import com.example.dogdemo.R;
import com.example.dogdemo.common.schedulers.SchedulerProvider;
import com.example.dogdemo.domain.managers.DogsManager;
import com.example.dogdemo.presentation.base.toolbar.BaseToolbarViewModel;
import com.example.dogdemo.presentation.mappers.DogsPresentationMapper;
import com.example.dogdemo.presentation.models.DogPresentation;
import com.example.dogdemo.presentation.ui.common.PaginationToolbarViewModel;
import com.example.dogdemo.presentation.utils.actions.UiEvents;
import com.example.dogdemo.presentation.utils.pagination.datasource.DogsListDataSource_Factory;
import com.example.dogdemo.utils.ResourceProvider;
import com.example.dogdemo.utils.pagination.DogsListDataSourceFactory;


import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import timber.log.Timber;

public class HomeFragmentViewModel extends PaginationToolbarViewModel<DogsListDataSourceFactory, DogPresentation> {
    private final DogsManager dogsManager;
    private ObservableList<DogPresentation> listOfDogs = new ObservableArrayList<>();
    private int page = 1;

    private boolean appsVisibility = true;

    @Inject
    public HomeFragmentViewModel(ResourceProvider resourceProvider,
                                 SchedulerProvider schedulerProvider, UiEvents uiEvents, DogsManager dogsManager) {
        super(resourceProvider, schedulerProvider, uiEvents);
        this.dogsManager = dogsManager;
        dataSourceFactory = new DogsListDataSourceFactory(dogsManager, getListener());

    }

    @Override
    public Drawable getToolbarImage() {
        return getResourceProvider().getDrawable(R.drawable.app_logo);
    }

    @Override
    public int getTitleVisibility() {
        return View.GONE;
    }

    @Override
    public int getImageVisibility() {
        return View.VISIBLE;
    }

    @Override
    protected int getPageSize() {
        return 5;
    }
}