package com.example.dogdemo.presentation.models;

import android.os.Parcel;
import android.os.Parcelable;

public class MeasuresPresentation implements Parcelable{
    private String imperial;
    private String metric;

    public MeasuresPresentation() {
    }

    public String getImperial() {
        return imperial;
    }

    public void setImperial(String imperial) {
        this.imperial = imperial;
    }

    public String getMetric() {
        return metric;
    }

    public void setMetric(String metric) {
        this.metric = metric;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.imperial);
        dest.writeString(this.metric);
    }

    protected MeasuresPresentation(Parcel in) {
        this.imperial = in.readString();
        this.metric = in.readString();
    }

    public static final Parcelable.Creator<MeasuresPresentation> CREATOR = new Parcelable.Creator<MeasuresPresentation>() {
        @Override
        public MeasuresPresentation createFromParcel(Parcel source) {
            return new MeasuresPresentation(source);
        }

        @Override
        public MeasuresPresentation[] newArray(int size) {
            return new MeasuresPresentation[size];
        }
    };
}
