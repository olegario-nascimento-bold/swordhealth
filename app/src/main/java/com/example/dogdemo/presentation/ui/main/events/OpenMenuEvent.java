package com.example.dogdemo.presentation.ui.main.events;

import com.example.dogdemo.presentation.ui.main.MenuType;
import com.example.dogdemo.presentation.utils.actions.events.Event;

public abstract class OpenMenuEvent extends Event {

    private MenuType menuType;
    private boolean clearStack;

    OpenMenuEvent(MenuType menuType, boolean clearStack) {
        this.menuType = menuType;
        this.clearStack = clearStack;
    }

    public MenuType getMenuType() {
        return menuType;
    }

    public boolean clearStack() {
        return clearStack;
    }
}