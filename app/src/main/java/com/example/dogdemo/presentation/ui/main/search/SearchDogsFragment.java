package com.example.dogdemo.presentation.ui.main.search;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.databinding.ObservableList;

import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.dogdemo.R;
import com.example.dogdemo.databinding.FragmentSearchDogsBinding;
import com.example.dogdemo.databinding.ListItemDogBinding;
import com.example.dogdemo.presentation.base.adapters.ObservablePagerAdapter;
import com.example.dogdemo.presentation.base.toolbar.BaseToolbarFragment;
import com.example.dogdemo.presentation.models.DogPresentation;
import com.example.dogdemo.presentation.ui.main.home.detail.DogDetailActivity;

import br.com.mauker.materialsearchview.MaterialSearchView;


public class SearchDogsFragment extends BaseToolbarFragment<FragmentSearchDogsBinding, SearchDogsViewModel> {

    public static SearchDogsFragment newInstance() {
        return new SearchDogsFragment();
    }

    private MaterialSearchView mMaterialSearchView;

    @Override
    protected int layoutResId() {
        return R.layout.fragment_search_dogs;
    }

    @Override
    protected Class<SearchDogsViewModel> viewModelClass() {
        return SearchDogsViewModel.class;
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    protected void initViews(FragmentSearchDogsBinding binding) {
        initToolbarWithMenu(binding.actionbar.toolbar, false, false, R.color.white, R.menu.menu_search, getMenuItemClickListener());
        //region MaterialSearchView
        mMaterialSearchView = binding.searchView;
        mMaterialSearchView.setVoiceIcon(0);

        mMaterialSearchView.setOnQueryTextListener(new MaterialSearchView.OnQueryTextListener() {

            @Override
            public boolean onQueryTextSubmit(String query) {
                getViewModel().onQueryTextSubmit(query);

                binding.fragmentSearchDogListRecyclerView.setAdapter(new SearchDogListAdapter(getViewModel().getDogsList()));

                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }


        });
        mMaterialSearchView.adjustTintAlpha(0.8f);
        //endregion

        //region onBackPressed
        getView().setFocusableInTouchMode(true);
        getView().requestFocus();
        getView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    mMaterialSearchView.closeSearch();
                    return false;
                }
                return false;
            }
        });
        //endregion

        binding.serviceError.findViewById(R.id.generic_error_button).setOnClickListener(
                v -> {
                    try {
                        mMaterialSearchView.clearAll();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                });
    }

    private Toolbar.OnMenuItemClickListener getMenuItemClickListener() {
        return item -> {
            switch (item.getItemId()) {
                case R.id.action_search:
                    mMaterialSearchView.openSearch();
                    return true;
                default:
                    return super.onOptionsItemSelected(item);
            }
        };
    }

    @Override
    protected void registerObservables() {
        getViewModel()
                .observeCloseSearchMenu()
                .doOnSubscribe(this::addDisposable)
                .subscribe(a ->
                        mMaterialSearchView.closeSearch()
                );
    }

    //endregion

    private class SearchDogListAdapter extends ObservablePagerAdapter<DogPresentation> {
        private LayoutInflater layoutInflater;

        SearchDogListAdapter(@Nullable ObservableList<DogPresentation> items) {
            super(items);
            layoutInflater = LayoutInflater.from(requireContext());
        }

        @NonNull
        @Override
        public Object instantiateItemObject(@NonNull ViewGroup container, int position) {

            ListItemDogBinding binding = ListItemDogBinding.inflate(layoutInflater, container, true);
            binding.setListItem(items.get(position));

            binding.getRoot().setOnClickListener(
                    v -> goToDogDetails(items.get(position))

            );
            return binding.getRoot();
        }
    }

    private void goToDogDetails(DogPresentation dogPresentation) {
        DogDetailActivity.launch(requireContext(), dogPresentation);
    }

}
