package com.example.dogdemo.presentation.models;

import android.os.Parcel;
import android.os.Parcelable;

public class ImagePresentation implements Parcelable {
    private String id;
    private String height;
    private String url;
    private String width;

    public ImagePresentation() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getHeight() {
        return height;
    }

    public void setHeight(String height) {
        this.height = height;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getWidth() {
        return width;
    }

    public void setWidth(String width) {
        this.width = width;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.id);
        dest.writeString(this.height);
        dest.writeString(this.url);
        dest.writeString(this.width);
    }

    protected ImagePresentation(Parcel in) {
        this.id = in.readString();
        this.height = in.readString();
        this.url = in.readString();
        this.width = in.readString();
    }

    public static final Parcelable.Creator<ImagePresentation> CREATOR = new Parcelable.Creator<ImagePresentation>() {
        @Override
        public ImagePresentation createFromParcel(Parcel source) {
            return new ImagePresentation(source);
        }

        @Override
        public ImagePresentation[] newArray(int size) {
            return new ImagePresentation[size];
        }
    };
}
