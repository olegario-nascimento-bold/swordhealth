package com.example.dogdemo.presentation.ui.main;

import androidx.databinding.Bindable;

import com.example.dogdemo.common.schedulers.SchedulerProvider;
import com.example.dogdemo.presentation.base.toolbar.BaseToolbarViewModel;
import com.example.dogdemo.presentation.utils.actions.UiEvents;
import com.example.dogdemo.utils.ResourceProvider;
import com.jakewharton.rxrelay2.BehaviorRelay;
import javax.inject.Inject;

public class MainViewModel extends BaseToolbarViewModel {
    private BehaviorRelay<Boolean> reloadAppObservable = BehaviorRelay.create();

    private MainNavigator mainNavigator;

    @Inject
    MainViewModel(ResourceProvider resourceProvider, SchedulerProvider schedulerProvider,
                  UiEvents uiEvents, MainNavigator mainNavigator) {
        super(resourceProvider, schedulerProvider, uiEvents);
        this.mainNavigator = mainNavigator;

    }

    @Bindable
    @Override
    public String getToolbarTitle() {
        return "";
    }

    public void onHomeClick() {
        mainNavigator.openHome();
    }

    public void onSearchClick() {
        mainNavigator.openSearch();
    }

    public MainNavigator navigator() {
        return mainNavigator;
    }

    public BehaviorRelay<Boolean> observeReloadApp() {
        return reloadAppObservable;
    }

}