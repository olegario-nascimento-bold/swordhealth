package com.example.dogdemo.presentation.ui.main;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.dogdemo.R;
import com.example.dogdemo.databinding.ActivityMainBinding;
import com.example.dogdemo.presentation.base.BaseFragment;
import com.example.dogdemo.presentation.base.toolbar.BaseToolbarActivity;
import com.example.dogdemo.presentation.ui.main.events.OpenMenuEvent;
import com.example.dogdemo.presentation.ui.main.home.HomeFragment;
import com.example.dogdemo.presentation.ui.main.search.SearchDogsFragment;
import com.google.android.material.bottomnavigation.BottomNavigationItemView;
import com.google.android.material.bottomnavigation.BottomNavigationMenuView;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

import com.example.dogdemo.utils.pagination.ServicesUtils;
import com.ncapdevi.fragnav.FragNavController;
import com.ncapdevi.fragnav.FragNavTransactionOptions;
import com.ncapdevi.fragnav.tabhistory.UnlimitedTabHistoryStrategy;


public class MainActivity extends BaseToolbarActivity<ActivityMainBinding, MainViewModel> {

    public static void launch(Context context) {
        context.startActivity(new Intent(context, MainActivity.class));
    }

    @Inject
    ServicesUtils mServicesUtils;

    private MenuType currentMenuType;

    private BottomNavigationView bottomNavigationView;

    private FragNavController fragNavController = new FragNavController(getSupportFragmentManager(), R.id.fragment_container);
    private FragNavTransactionOptions fragNavTransactionOptions = new FragNavTransactionOptions.Builder()
            .customAnimations(R.anim.enter_slide_left, R.anim.exit_slide_left, R.anim.enter_slide_right, R.anim.exit_slide_right)
            .build();

    private BottomNavigationView.OnNavigationItemSelectedListener onNavigationItemSelectedListener
            = item -> {
        switch (item.getItemId()) {
            case R.id.navigation_home:
                getViewModel().onHomeClick();
                return true;
            case R.id.navigation_search:
                getViewModel().onSearchClick();
                return true;
        }
        return false;
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initNavigation(savedInstanceState);
    }

    private void initNavigation(Bundle savedInstanceState) {
        fragNavController.setFragmentHideStrategy(FragNavController.DETACH_ON_NAVIGATE_HIDE_ON_SWITCH);
        fragNavController.setNavigationStrategy(new UnlimitedTabHistoryStrategy((index, fragNavTransactionOptions) -> {
            switch (index) {
                case FragNavController.TAB1:
                    getViewModel().onHomeClick();
                    break;
                case FragNavController.TAB2:
                    getViewModel().onSearchClick();
                    break;
            }
        }));

        List<Fragment> fragments = new ArrayList<Fragment>(5) {{
            add(HomeFragment.newInstance());
            add(SearchDogsFragment.newInstance());
        }};

        FragNavTransactionOptions options = new FragNavTransactionOptions.Builder()
                .transition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                .build();
        fragNavController.setDefaultTransactionOptions(options);

        fragNavController.setRootFragments(fragments);

        fragNavController.initialize(FragNavController.TAB1, savedInstanceState);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        fragNavController.onSaveInstanceState(outState);
    }

    @Override
    public int layoutResId() {
        return R.layout.activity_main;
    }

    @Override
    protected void initViews(ActivityMainBinding binding) {
        //ViewsHelper.disableShiftMode(binding.navigation);
        binding.navigation.setItemIconTintList(null);
        binding.navigation.setOnNavigationItemSelectedListener(onNavigationItemSelectedListener);
        bottomNavigationView = binding.navigation;

        //region Just a fix for long names on the BottomNavigationView. Google must fix this!
        BottomNavigationMenuView menuView = (BottomNavigationMenuView) bottomNavigationView.getChildAt(0);
        for (int i = 0; i < menuView.getChildCount(); i++) {
            BottomNavigationItemView item = (BottomNavigationItemView) menuView.getChildAt(i);
            View activeLabel = item.findViewById(R.id.largeLabel);
            if (activeLabel instanceof TextView) {
                activeLabel.setPadding(0, 0, 0, 0);
            }
        }
        //endregion
    }

    @SuppressLint("CheckResult")
    @Override
    protected void registerObservables() {
        super.registerObservables();
        getViewModel().navigator().observeOpenMenu()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(this::addDisposable)
                .subscribe(this::openMenu);

        getViewModel().navigator()
                .observeCloseSubScreen()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(this::addDisposable)
                .subscribe(event -> popFragment());
    }

    @Override
    public Class<MainViewModel> viewModelClass() {
        return MainViewModel.class;
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        FragmentManager fragmentManager = getSupportFragmentManager();
        final boolean isStateSaved = fragmentManager.isStateSaved();

        if (isStateSaved && Build.VERSION.SDK_INT <= Build.VERSION_CODES.N_MR1) {
            // Older versions will throw an exception from the framework
            // FragmentManager.popBackStackImmediate(), so we'll just
            // return here. The Activity is likely already on its way out
            // since the fragmentManager has already been saved.
            return;
        }
    }

    private void openMenu(OpenMenuEvent event) {
        MenuType menuType = event.getMenuType();
        if (currentMenuType == menuType && !event.clearStack()) {
            return;
        }

        switch (menuType) {
            case HOME:
            default:
                switchTab(FragNavController.TAB1, event.clearStack());
                break;
            case SEARCH:
                switchTab(FragNavController.TAB2, event.clearStack());
                break;
        }

        updateNavigation(menuType);

        this.currentMenuType = menuType;
    }

    private BaseFragment getFragment() {
        return (BaseFragment) fragNavController.getCurrentFrag();
    }

    private void switchTab(int index, boolean clearStack) {
        fragNavController.switchTab(index);

        if (clearStack) {
            fragNavController.clearStack();
        }
    }

    private void pushFragment(Fragment fragment) {
        fragNavController.pushFragment(fragment, fragNavTransactionOptions);
    }

    private void popFragment() {
        fragNavController.popFragment(fragNavTransactionOptions);
    }

    private void updateNavigation(MenuType menuType) {
        getBinding().navigation.setOnNavigationItemSelectedListener(null);
        switch (menuType) {
            case HOME:
                getBinding().navigation.setSelectedItemId(R.id.navigation_home);
                break;
            case SEARCH:
                getBinding().navigation.setSelectedItemId(R.id.navigation_search);
                break;
        }
        getBinding().navigation.setOnNavigationItemSelectedListener(onNavigationItemSelectedListener);
    }
}