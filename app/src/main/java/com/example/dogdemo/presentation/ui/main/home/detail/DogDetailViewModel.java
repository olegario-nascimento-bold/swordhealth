package com.example.dogdemo.presentation.ui.main.home.detail;


import androidx.databinding.Bindable;

import com.example.dogdemo.BR;
import com.example.dogdemo.common.schedulers.SchedulerProvider;
import com.example.dogdemo.presentation.base.toolbar.BaseToolbarViewModel;
import com.example.dogdemo.presentation.models.DogPresentation;
import com.example.dogdemo.presentation.utils.actions.UiEvents;
import com.example.dogdemo.utils.ResourceProvider;

import java.net.URL;

import javax.inject.Inject;

public class DogDetailViewModel extends BaseToolbarViewModel {
    private DogPresentation dog;
    private String name;
    private String category;
    private String temperament;
    private String origin;
    private String image;

    @Inject
    public DogDetailViewModel(ResourceProvider resourceProvider, SchedulerProvider schedulerProvider, UiEvents uiEvents,
                                DogPresentation eventPresentation) {
        super(resourceProvider, schedulerProvider, uiEvents);

        this.dog = eventPresentation;
        setName(dog.getName());
        setCategory(dog.getBreedGroup());
        setTemperament(dog.getTemperament());
        setOrigin(dog.getOrigin());
        setImage(dog.getImage().getUrl());
    }

    @Override
    public String getToolbarTitle() {
        return dog.getName();
    }

    @Bindable
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
        notifyPropertyChanged(BR.name);
    }

    @Bindable
    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
        notifyPropertyChanged(BR.category);
    }

    @Bindable
    public String getTemperament() {
        return temperament;
    }

    public void setTemperament(String temperament) {
        this.temperament = temperament;
        notifyPropertyChanged(BR.temperament);
    }

    @Bindable
    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
        notifyPropertyChanged(BR.origin);
    }

    @Bindable
    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
        notifyPropertyChanged(BR.image);
    }

    public DogPresentation getEvent() {
        return dog;
    }

}