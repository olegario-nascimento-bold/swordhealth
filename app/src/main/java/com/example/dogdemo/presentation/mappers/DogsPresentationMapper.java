package com.example.dogdemo.presentation.mappers;

import com.example.dogdemo.domain.models.Dog;
import com.example.dogdemo.presentation.models.DogPresentation;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper
public interface DogsPresentationMapper {
    DogsPresentationMapper INSTANCE = Mappers.getMapper(DogsPresentationMapper.class);

    //map to domain
    DogPresentation dogsDomainToPresentation(Dog dogsDomain);
    List<DogPresentation> listOfDogsDomainToPresentation(List<Dog> appsDomain);

}