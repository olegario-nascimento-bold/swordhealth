package com.example.dogdemo.presentation.ui.main.home;

import android.widget.LinearLayout;

import androidx.recyclerview.widget.LinearLayoutManager;

import com.example.dogdemo.R;
import com.example.dogdemo.databinding.FragmentHomeBinding;
import com.example.dogdemo.presentation.base.loading.BaseLoadingToolbarFragment;
import com.example.dogdemo.presentation.ui.main.home.detail.DogDetailActivity;
import com.example.dogdemo.presentation.views.LoadingView;
import com.example.dogdemo.presentation.views.VerticalItemSpacer;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import timber.log.Timber;

public class HomeFragment extends BaseLoadingToolbarFragment<FragmentHomeBinding, HomeFragmentViewModel> {

    public static HomeFragment newInstance() {
        return new HomeFragment();
    }

    LinearLayout dogLogo;
    private DogListAdapter adapter;

    @Override
    protected int layoutResId() {
        return R.layout.fragment_home;
    }

    @Override
    protected Class<HomeFragmentViewModel> viewModelClass() {
        return HomeFragmentViewModel.class;
    }

    @Override
    protected void initViews(FragmentHomeBinding binding) {

        initToolbarWithoutNavigation(binding.actionbar.toolbar);

        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        adapter = new DogListAdapter(dog ->
                DogDetailActivity.launch(
                        getActivity(), dog
                ),
                getResourceProvider()
        );
        binding.activityEventListRecyclerView.setLayoutManager(layoutManager);
        binding.activityEventListRecyclerView.setAdapter(adapter);
        binding.activityEventListRecyclerView.addItemDecoration(new VerticalItemSpacer());
        submitItems();
        this.dogLogo = binding.actionbar.appBarLogo;
    }

    @Override
    protected void registerObservables() {
        super.registerObservables();
        getViewModel().observeRecyclerLoading()
                .doOnSubscribe(this::addDisposable)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        load -> {
                            if (adapter != null) {
                                adapter.setLoading(load);
                            }
                        }, error -> {
                            Timber.e(error);
                            adapter.setLoading(false);
                        }
                );

        getViewModel().observeClearData()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        event -> {
                            getViewModel().clearDataSource();
                            adapter.notifyDataSetChanged();
                            submitItems();
                        }, Timber::e
                );
    }

    public void submitItems() {
        getViewModel().getItems()
                .subscribe(
                        list -> adapter.submitList(list)
                );


    }

    @Override
    protected LoadingView getLoadingViewBinding() {
        return getBinding().fragmentDogListLoadingView;
    }
}