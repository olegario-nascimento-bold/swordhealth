package com.example.dogdemo.presentation.utils.pagination.datasource;

import android.annotation.SuppressLint;

import androidx.annotation.NonNull;
import androidx.paging.PageKeyedDataSource;

import com.example.dogdemo.di.scopes.ActivityScope;
import com.example.dogdemo.domain.managers.DogsManager;
import com.example.dogdemo.presentation.mappers.DogsPresentationMapper;
import com.example.dogdemo.presentation.models.DogPresentation;
import com.example.dogdemo.presentation.utils.pagination.datasource.base.BaseDataSource;

import java.util.Collections;

import javax.inject.Inject;

import io.reactivex.schedulers.Schedulers;

@ActivityScope
public class DogsListDataSource extends BaseDataSource<DogPresentation> {

    @Inject
    public DogsListDataSource(DogsManager dogsManager) {
        this.dogsManager = dogsManager;
    }

    DogsManager dogsManager;

    @SuppressLint("CheckResult")
    @Override
    protected void loadInitialData(@NonNull PageKeyedDataSource.LoadInitialParams<Integer> params, @NonNull PageKeyedDataSource.LoadInitialCallback<Integer, DogPresentation> callback) {
        dogsManager.getDogs("", 0, params.requestedLoadSize)
                .map(DogsPresentationMapper.INSTANCE::listOfDogsDomainToPresentation)
                .subscribeOn(Schedulers.io())
                .subscribe(
                        items -> {
                            submitInitialData(items, params, callback);
                        },
                        error -> submitInitialError(error)
                );
    }

    @SuppressLint("CheckResult")
    @Override
    protected void loadAditionalData(@NonNull PageKeyedDataSource.LoadParams<Integer> params, @NonNull PageKeyedDataSource.LoadCallback<Integer, DogPresentation> callback) {
        dogsManager.getDogs("", params.key, params.requestedLoadSize)
                .map(DogsPresentationMapper.INSTANCE::listOfDogsDomainToPresentation)
                .subscribe(
                        items -> submitData(items, params, callback),
                        error -> submitError(error)
                );
    }
}