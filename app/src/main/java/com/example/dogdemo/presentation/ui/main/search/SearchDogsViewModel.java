package com.example.dogdemo.presentation.ui.main.search;

import android.annotation.SuppressLint;

import androidx.databinding.Bindable;
import androidx.databinding.ObservableArrayList;
import androidx.databinding.ObservableList;
import androidx.databinding.library.baseAdapters.BR;

import com.example.dogdemo.R;
import com.example.dogdemo.common.schedulers.SchedulerProvider;
import com.example.dogdemo.domain.managers.DogsManager;
import com.example.dogdemo.domain.models.Dog;
import com.example.dogdemo.presentation.base.toolbar.BaseToolbarViewModel;
import com.example.dogdemo.presentation.mappers.DogsPresentationMapper;
import com.example.dogdemo.presentation.models.DogPresentation;
import com.example.dogdemo.presentation.utils.actions.UiEvents;
import com.example.dogdemo.utils.ResourceProvider;
import com.jakewharton.rxrelay2.BehaviorRelay;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import timber.log.Timber;

public class SearchDogsViewModel extends BaseToolbarViewModel {

    private DogsManager dogsManager;
    private BehaviorRelay<List<DogPresentation>> dogsObservable = BehaviorRelay.create();
    private BehaviorRelay<Boolean> closeSearhMenuObservable = BehaviorRelay.create();
    private boolean genericErrorVisible = false;
    private ObservableList<DogPresentation> dogsList = new ObservableArrayList<>();

    @Inject
    SearchDogsViewModel(ResourceProvider resourceProvider, SchedulerProvider schedulerProvider,
                        UiEvents uiEvents, DogsManager dogsManager) {
        super(resourceProvider, schedulerProvider, uiEvents);
        this.dogsManager = dogsManager;
        setEmptyList(false);
    }

    public void onQueryTextSubmit(String query) {
        dogsManager.searchDogs(query)
                .doOnSubscribe(this::addDisposable)
                .subscribeOn(Schedulers.io())
                .map(DogsPresentationMapper.INSTANCE::listOfDogsDomainToPresentation)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(dogItems -> {
                    this.dogsList.addAll(dogItems);
                    if(this.dogsList.size() < 1)
                        setEmptyList(true);
                    else
                        setEmptyList(false);
                }, error -> {
                    Timber.e(error);
                });
    }

    public ObservableList<DogPresentation> getDogsList() {
        return this.dogsList;
    }

    @Override
    public String getToolbarTitle() {
        return getResourceProvider().getString(R.string.lbl_search_dogs_tab);
    }

    @Bindable
    public boolean getEmptyList() {
        return genericErrorVisible;
    }

    public void setEmptyList(boolean visible) {
        notifyPropertyChanged(BR.emptyList);
        this.genericErrorVisible = visible;
    }

    public Observable<Boolean> observeCloseSearchMenu() {
        return closeSearhMenuObservable;
    }
}