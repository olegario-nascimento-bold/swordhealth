package com.example.dogdemo.presentation.models;

import android.os.Parcel;
import android.os.Parcelable;

public class DogPresentation implements Parcelable {

    private String id;
    private String bredFor;
    private String breedGroup;
    private MeasuresPresentation height;
    private String lifeSpan;
    private String name;
    private String origin;
    private String temperament;
    private MeasuresPresentation weight;
    private ImagePresentation image;


    public DogPresentation() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getBredFor() {
        return bredFor;
    }

    public void setBredFor(String bredFor) {
        this.bredFor = bredFor;
    }

    public String getBreedGroup() {
        return breedGroup;
    }

    public void setBreedGroup(String breedGroup) {
        this.breedGroup = breedGroup;
    }

    public MeasuresPresentation getHeight() {
        return height;
    }

    public void setHeight(MeasuresPresentation height) {
        this.height = height;
    }

    public String getLifeSpan() {
        return lifeSpan;
    }

    public void setLifeSpan(String lifeSpan) {
        this.lifeSpan = lifeSpan;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public String getTemperament() {
        return temperament;
    }

    public void setTemperament(String temperament) {
        this.temperament = temperament;
    }

    public MeasuresPresentation getWeight() {
        return weight;
    }

    public void setWeight(MeasuresPresentation weight) {
        this.weight = weight;
    }

    public ImagePresentation getImage() {
        return image;
    }

    public void setImage(ImagePresentation image) {
        this.image = image;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.id);
        dest.writeString(this.bredFor);
        dest.writeString(this.breedGroup);
        dest.writeParcelable(this.height, flags);
        dest.writeString(this.name);
        dest.writeString(this.origin);
        dest.writeString(this.temperament);
        dest.writeParcelable(this.weight, flags);
        dest.writeParcelable(this.image, flags);
    }

    protected DogPresentation(Parcel in) {
        this.id = in.readString();
        this.bredFor = in.readString();
        this.breedGroup = in.readString();
        this.height = in.readParcelable(MeasuresPresentation.class.getClassLoader());
        this.name = in.readString();
        this.origin = in.readString();
        this.temperament = in.readString();
        this.weight = in.readParcelable(MeasuresPresentation.class.getClassLoader());
        this.image = in.readParcelable(ImagePresentation.class.getClassLoader());
    }

    public static final Creator<DogPresentation> CREATOR = new Creator<DogPresentation>() {
        @Override
        public DogPresentation createFromParcel(Parcel source) {
            return new DogPresentation(source);
        }

        @Override
        public DogPresentation[] newArray(int size) {
            return new DogPresentation[size];
        }
    };
}
