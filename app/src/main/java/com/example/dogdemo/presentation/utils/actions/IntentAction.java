package com.example.dogdemo.presentation.utils.actions;

import java.util.concurrent.atomic.AtomicBoolean;

import io.reactivex.functions.Consumer;

public class IntentAction<T> {

    public static <T> IntentAction<T> of(T value) {
        return new IntentAction<>(value);
    }

    private T value;
    private AtomicBoolean handled = new AtomicBoolean(false);

    private IntentAction(T value) {
        this.value = value;
    }

    public void handle(Consumer<T> consumer) throws Exception {
        if (!handled.get()) {
            consumer.accept(value);
            handled.set(true);
        }
    }

    public boolean isHandled() {
        return handled.get();
    }

    public T peek() {
        return value;
    }
}