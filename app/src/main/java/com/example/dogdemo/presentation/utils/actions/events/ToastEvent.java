package com.example.dogdemo.presentation.utils.actions.events;

public class ToastEvent extends Event {

    private String mMessage;

    public ToastEvent(String message) {
        mMessage = message;
    }

    public String getMessage() {
        return mMessage;
    }
}