package com.example.dogdemo.presentation.ui.main.events;

import com.example.dogdemo.presentation.ui.main.MenuType;

public class OpenSearchEvent extends OpenMenuEvent {

    public OpenSearchEvent(boolean clearStack) {
        super(MenuType.SEARCH, clearStack);
    }
}

