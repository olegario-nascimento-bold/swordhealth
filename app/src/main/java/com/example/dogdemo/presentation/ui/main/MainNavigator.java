package com.example.dogdemo.presentation.ui.main;


import com.example.dogdemo.presentation.ui.main.events.OpenHomeEvent;
import com.example.dogdemo.presentation.ui.main.events.OpenMenuEvent;
import com.example.dogdemo.presentation.ui.main.events.OpenSearchEvent;
import com.example.dogdemo.presentation.utils.actions.IntentActionManager;
import com.example.dogdemo.presentation.utils.actions.events.CloseEvent;
import com.example.dogdemo.presentation.utils.actions.events.NavigationEvent;
import com.jakewharton.rxrelay2.BehaviorRelay;
import com.jakewharton.rxrelay2.PublishRelay;

import javax.inject.Inject;

import io.reactivex.Observable;

public class MainNavigator {

    private IntentActionManager intentActionManager;

    private BehaviorRelay<OpenMenuEvent> openMenuObservable = BehaviorRelay.create();
    private PublishRelay<NavigationEvent> openSubScreenObservable = PublishRelay.create();
    private PublishRelay<CloseEvent> closeSubScreenObservable = PublishRelay.create();

    @Inject
    MainNavigator(IntentActionManager intentActionManager) {
        this.intentActionManager = intentActionManager;
        openHome(true);
    }

    public MenuType getCurrentMenuType() {
        return openMenuObservable.getValue().getMenuType();
    }

    //<editor-fold desc="Menu Navigation">
    public void openHome() {
        openHome(false);
    }

    public void openHome(boolean clearStack) {
        openMenu(new OpenHomeEvent(clearStack));
    }

    public void openSearch() {
        openSearch(false);
    }

    public void openSearch(boolean clearStack) {
        openMenu(new OpenSearchEvent(clearStack));
    }

    private void openMenu(OpenMenuEvent event) {
        OpenMenuEvent menuEvent = openMenuObservable.getValue();
        if (menuEvent == null || menuEvent.getMenuType() != event.getMenuType() || event.clearStack()) {
            openMenuObservable.accept(event);
        }
    }
    //</editor-fold>

    public void closeSubScreen() {
        closeSubScreenObservable.accept(new CloseEvent());
    }
    //</editor-fold>

    Observable<OpenMenuEvent> observeOpenMenu() {
        return openMenuObservable;
    }

    Observable<NavigationEvent> observeOpenSubScreen() {
        return openSubScreenObservable;
    }

    Observable<CloseEvent> observeCloseSubScreen() {
        return closeSubScreenObservable;
    }
}