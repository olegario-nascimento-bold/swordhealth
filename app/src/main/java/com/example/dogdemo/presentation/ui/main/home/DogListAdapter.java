package com.example.dogdemo.presentation.ui.main.home;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.databinding.ViewDataBinding;

import com.example.dogdemo.databinding.ListItemDogBinding;
import com.example.dogdemo.presentation.base.adapters.BindablePagedDiffAdapter;
import com.example.dogdemo.presentation.base.adapters.BindableViewHolder;
import com.example.dogdemo.presentation.models.DogPresentation;
import com.example.dogdemo.utils.ResourceProvider;

public class DogListAdapter extends BindablePagedDiffAdapter<DogPresentation> {

    private final DogItemClickListener itemClickListener;
    private final ResourceProvider resourceProvider;

    interface DogItemClickListener {
        void onDogItemClicked(DogPresentation event);
    }

    public DogListAdapter(DogItemClickListener listener, ResourceProvider resourceProvider) {
        super();
        this.itemClickListener = listener;
        this.resourceProvider = resourceProvider;
    }

    @Override
    public BindableViewHolder<? extends DogPresentation, ? extends ViewDataBinding> onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
            LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
            ListItemDogBinding binding = ListItemDogBinding.inflate(layoutInflater, parent, false);
            return new DogViewHolder(binding);
    }

    class DogViewHolder extends BindableViewHolder<DogPresentation, ListItemDogBinding> {

        public DogViewHolder(ListItemDogBinding binding) {
            super(binding);
        }

        @Override
        public void bind(DogPresentation item) {
            super.bind(item);
            binding.setListItem(item);
            binding.listItemDogsCardView.setClipToOutline(true);
            binding.listItemDogsRoot.setOnClickListener(
                    (v) -> {
                        if (itemClickListener != null) {
                            itemClickListener.onDogItemClicked(item);
                        }
                    }
            );
        }
    }


}
