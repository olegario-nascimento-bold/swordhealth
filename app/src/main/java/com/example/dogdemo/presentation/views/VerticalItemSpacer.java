package com.example.dogdemo.presentation.views;

import android.graphics.Rect;
import android.view.View;

import androidx.recyclerview.widget.RecyclerView;

import com.example.dogdemo.utils.ViewsHelper;

public class VerticalItemSpacer extends RecyclerView.ItemDecoration {
    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
        super.getItemOffsets(outRect, view, parent, state);
        outRect.top = ViewsHelper.convertDpToPx(parent.getContext(), 10);
    }
}