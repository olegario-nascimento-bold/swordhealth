package com.example.dogdemo.presentation.utils.transitions;

import com.example.dogdemo.presentation.utils.ActivityTransition;

public class NoTransition extends ActivityTransition {

    public NoTransition() {
        super(0, 0, 0, 0);
    }
}