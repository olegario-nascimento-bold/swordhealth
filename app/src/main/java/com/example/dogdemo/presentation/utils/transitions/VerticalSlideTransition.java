package com.example.dogdemo.presentation.utils.transitions;

import com.example.dogdemo.R;
import com.example.dogdemo.presentation.utils.ActivityTransition;

public class VerticalSlideTransition extends ActivityTransition {

    public VerticalSlideTransition() {
        super(R.anim.enter_slide_up, R.anim.stay, R.anim.stay, R.anim.leave_slide_down);
    }
}