package com.example.dogdemo.presentation.utils.pagination.datasource.base;

public interface OnDataSourceLoading {
    void onFirstFetch();
    void onFirstFetchEndWithData();
    void onFirstFetchEndWithoutData();
    void onDataLoading();
    void onDataLoadingEnd();
    void onInitialError();
    void onError();
}