package com.example.dogdemo.presentation.base.adapters;


import androidx.annotation.CallSuper;
import androidx.annotation.NonNull;
import androidx.databinding.ViewDataBinding;
import androidx.paging.PagedListAdapter;
import androidx.recyclerview.widget.DiffUtil;

public abstract class BindablePagedDiffAdapter<T> extends PagedListAdapter<T, BindableViewHolder<? extends T, ? extends ViewDataBinding>> {

    public boolean loading;
    public static final int VIEW_TYPE_NORMAL = 0;
    public static final int VIEW_TYPE_LOADING = 1;

    public void setLoading(boolean loading) {
        this.loading = loading;
        if (!loading) {
            notifyDataSetChanged();
        }
    }

    public BindablePagedDiffAdapter() {
        super(defaultDiffCallback());
    }

    public BindablePagedDiffAdapter(@NonNull DiffUtil.ItemCallback<T> diffCallback) {
        super(diffCallback);
    }

    @Override
    public void onBindViewHolder(@NonNull BindableViewHolder<? extends T, ? extends ViewDataBinding> holder, int position) {
        if (getItemViewType(position) == VIEW_TYPE_NORMAL) {
            holder.bindInternal(getItem(position));
        }
    }

    @CallSuper
    @Override
    public void onViewRecycled(@NonNull BindableViewHolder<? extends T, ? extends ViewDataBinding> holder) {
        holder.unbind();
    }

    private static <T> DiffUtil.ItemCallback<T> defaultDiffCallback() {
        return new DiffUtil.ItemCallback<T>() {
            @Override
            public boolean areItemsTheSame(T oldItem, T newItem) {
                return oldItem.equals(newItem);
            }

            @Override
            public boolean areContentsTheSame(T oldItem, T newItem) {
                return oldItem.equals(newItem);
            }
        };
    }

    @Override
    public int getItemViewType(int position) {
        if (loading && position == getItemCount() - 1) {
            return VIEW_TYPE_LOADING;
        } else {
            return VIEW_TYPE_NORMAL;
        }
    }

}