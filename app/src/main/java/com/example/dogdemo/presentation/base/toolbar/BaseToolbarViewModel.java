package com.example.dogdemo.presentation.base.toolbar;


import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.view.View;

import androidx.databinding.Bindable;

import com.example.dogdemo.R;
import com.example.dogdemo.common.schedulers.SchedulerProvider;
import com.example.dogdemo.presentation.base.BaseViewModel;
import com.example.dogdemo.presentation.utils.actions.UiEvents;
import com.example.dogdemo.utils.ResourceProvider;

public abstract class BaseToolbarViewModel extends BaseViewModel {

    public BaseToolbarViewModel(ResourceProvider resourceProvider, SchedulerProvider schedulerProvider, UiEvents uiEvents) {
        super(resourceProvider, schedulerProvider, uiEvents);
    }

    @Bindable
    public String getToolbarTitle() {
        return "";
    }

    @Bindable
    public Drawable getToolbarImage() {
        Drawable transparentDrawable = new ColorDrawable(Color.TRANSPARENT);
        return transparentDrawable;
    }

    @Bindable
    public int getToolbarTextColor() {
        return R.color.teal_200;
    }

    @Bindable
    public int getTitleVisibility() {
        return View.VISIBLE;
    }

    @Bindable
    public int getImageVisibility() {
        return View.GONE;
    }

    public void onAppLogoClicked() {

    }

    @Bindable
    public int getImageScale() {
        return 1;
    }

}