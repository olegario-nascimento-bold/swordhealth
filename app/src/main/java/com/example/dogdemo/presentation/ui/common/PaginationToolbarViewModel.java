package com.example.dogdemo.presentation.ui.common;

import androidx.databinding.Bindable;
import androidx.databinding.library.baseAdapters.BR;
import androidx.paging.DataSource;
import androidx.paging.PagedList;
import androidx.paging.RxPagedListBuilder;

import com.example.dogdemo.common.schedulers.SchedulerProvider;
import com.example.dogdemo.presentation.base.loading.BaseLoadingToolbarViewModel;
import com.example.dogdemo.presentation.ui.main.home.ResetAdapterDog;
import com.example.dogdemo.presentation.utils.actions.UiEvents;
import com.example.dogdemo.presentation.utils.pagination.datasource.base.OnDataSourceLoading;
import com.example.dogdemo.utils.ResourceProvider;
import com.jakewharton.rxrelay2.PublishRelay;

import io.reactivex.Observable;

public abstract class PaginationToolbarViewModel<T extends DataSource.Factory<Integer, K>, K> extends BaseLoadingToolbarViewModel {


    //region Pagination
    private Observable<PagedList<K>> pagedObservable;
    private PublishRelay<Boolean> fetchNewDataLoading = PublishRelay.create();
    protected PublishRelay<ResetAdapterDog> clearDataObservable = PublishRelay.create();
    private boolean emptyVisibility = false;
    protected T dataSourceFactory;
    protected abstract int getPageSize();
    //endregion

    public PaginationToolbarViewModel(ResourceProvider resourceProvider,
                                      SchedulerProvider schedulerProvider, UiEvents uiEvents) {
        super(resourceProvider, schedulerProvider, uiEvents);
    }

    //region pagination
    public Observable<Boolean> observeRecyclerLoading() { return fetchNewDataLoading; }

    public Observable<PagedList<K>> getItems() {
        if (pagedObservable == null) {
            createPagedObservable();
        }
        return pagedObservable;
    }

    public void createPagedObservable() {
        pagedObservable = new RxPagedListBuilder(
                dataSourceFactory,
                new PagedList.Config.Builder()
                        .setPageSize(getPageSize())
                        .setEnablePlaceholders(false)
                        .build())
                .buildObservable();
    }

    public void clearDataSource() {
        dataSourceFactory.create();
        createPagedObservable();
    }

    public void setEmptyVisibility(boolean emptyVisibility) {
        this.emptyVisibility = emptyVisibility;
        notifyPropertyChanged(BR.emptyVisibility);
    }


    @Bindable
    public boolean isEmptyVisibility() {
        return emptyVisibility;
    }

    protected OnDataSourceLoading getListener() {
        return new OnDataSourceLoading() {
            @Override
            public void onFirstFetch() {
                showLoading();
                fetchNewDataLoading.accept(true);
            }

            @Override
            public void onFirstFetchEndWithData() {
                hideLoading();
                setEmptyVisibility(false);
                fetchNewDataLoading.accept(false);
            }

            @Override
            public void onFirstFetchEndWithoutData() {
                hideLoading();
                setEmptyVisibility(true);
                fetchNewDataLoading.accept(false);
            }

            @Override
            public void onDataLoading() {
                fetchNewDataLoading.accept(true);
            }

            @Override
            public void onDataLoadingEnd() {
                hideLoading();
                fetchNewDataLoading.accept(false);
            }

            @Override
            public void onInitialError() {
                hideLoading();
                setEmptyVisibility(true);
            }

            @Override
            public void onError() {
                hideLoading();
            }
        };
    }

    public Observable<ResetAdapterDog> observeClearData() {
        return clearDataObservable;
    }

    //endregion
}
