package com.example.dogdemo.presentation.base.adapters;

import android.util.SparseArray;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;

import com.example.dogdemo.presentation.views.WrapContentViewPager;

public abstract class ObjectAtPositionPagerAdapter extends PagerAdapter implements WrapContentViewPager.ObjectAtPositionInterface {
    protected SparseArray<Object> objects = new SparseArray<>();

    @NonNull
    @Override
    public final Object instantiateItem(@NonNull ViewGroup container, int position) {
        Object object = instantiateItemObject(container, position);
        objects.put(position, object);
        return object;
    }

    /**
     * Replaces @see PagerAdapter#instantiateItem and handles objects tracking for getObjectAtPosition
     */
    @NonNull
    public abstract Object instantiateItemObject(@NonNull ViewGroup container, int position);

    @Override
    public final void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        objects.remove(position);
        destroyItemObject(container, position, object);
    }

    /**
     * Replaces @see PagerAdapter#destroyItem and handles objects tracking for getObjectAtPosition
     */
    public abstract void destroyItemObject(@NonNull ViewGroup container, int position, @NonNull Object object);

    @Override
    public Object getObjectAtPosition(int position) {
        return objects.get(position);
    }
}