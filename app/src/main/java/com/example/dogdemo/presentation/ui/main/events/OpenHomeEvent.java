package com.example.dogdemo.presentation.ui.main.events;

import com.example.dogdemo.presentation.ui.main.MenuType;

public class OpenHomeEvent extends OpenMenuEvent {

    public OpenHomeEvent(boolean clearStack) {
        super(MenuType.HOME, clearStack);
    }
}