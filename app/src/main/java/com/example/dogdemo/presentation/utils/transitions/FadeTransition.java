package com.example.dogdemo.presentation.utils.transitions;

import com.example.dogdemo.R;
import com.example.dogdemo.presentation.utils.ActivityTransition;

public class FadeTransition extends ActivityTransition {

    public FadeTransition() {
        super(R.anim.enter_fade, R.anim.stay, R.anim.stay, R.anim.exit_fade);
    }
}
