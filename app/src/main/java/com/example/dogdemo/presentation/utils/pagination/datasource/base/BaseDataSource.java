package com.example.dogdemo.presentation.utils.pagination.datasource.base;

import androidx.annotation.CallSuper;
import androidx.annotation.NonNull;
import androidx.paging.PageKeyedDataSource;

import java.util.List;

import timber.log.Timber;

public abstract class BaseDataSource<T> extends PageKeyedDataSource<Integer, T> {
    protected OnDataSourceLoading onDataSourceLoading;

    public void setOnDataSourceLoading(OnDataSourceLoading onDataSourceLoading) {
        this.onDataSourceLoading = onDataSourceLoading;
    }

    protected abstract void loadInitialData(@NonNull PageKeyedDataSource.LoadInitialParams<Integer> params, @NonNull LoadInitialCallback<Integer, T> callback);
    protected abstract void loadAditionalData(@NonNull LoadParams<Integer> params, @NonNull LoadCallback<Integer, T> callback);


    @CallSuper
    @Override
    public void loadInitial(@NonNull LoadInitialParams<Integer> params, @NonNull LoadInitialCallback<Integer, T> callback) {
        if (onDataSourceLoading != null) {
            onDataSourceLoading.onFirstFetch();
        }
        loadInitialData(params, callback);
    }

    @Override
    public void loadBefore(@NonNull LoadParams<Integer> params, @NonNull LoadCallback<Integer, T> callback) {

    }

    @CallSuper
    @Override
    public void loadAfter(@NonNull LoadParams<Integer> params, @NonNull LoadCallback<Integer, T> callback) {
        if (onDataSourceLoading != null) {
            onDataSourceLoading.onDataLoading();
        }
        loadAditionalData(params, callback);
    }

    protected void submitInitialData (List<T> items, LoadInitialParams<Integer> params, @NonNull LoadInitialCallback<Integer, T> callback) {
        callback.onResult(items, 0, params.requestedLoadSize);
        if(onDataSourceLoading != null) {
            if (items != null && !items.isEmpty()) {
                onDataSourceLoading.onFirstFetchEndWithData();
            } else {
                onDataSourceLoading.onFirstFetchEndWithoutData();
            }
        }
    }

    protected void submitData(List<T> items, LoadParams<Integer> params, @NonNull LoadCallback<Integer, T> callback) {
        // If we reach the limit, then put value NULL as adjacent key to state that the list has ended
        // else, we will configure the next key to be fetched
        callback.onResult(items, (items == null || items.isEmpty()) ? null : params.key + items.size());
        if (onDataSourceLoading != null) {
            onDataSourceLoading.onDataLoadingEnd();
        }
    }

    protected void submitInitialError(Throwable error) {
        Timber.e(error);
        onDataSourceLoading.onError();
    }


    protected void submitError(Throwable error) {
        Timber.e(error);
        onDataSourceLoading.onError();
    }
}
