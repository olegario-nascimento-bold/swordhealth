package com.example.dogdemo.presentation.base.loading;

import com.example.dogdemo.common.schedulers.SchedulerProvider;
import com.example.dogdemo.presentation.base.toolbar.BaseToolbarViewModel;
import com.example.dogdemo.presentation.utils.actions.UiEvents;
import com.example.dogdemo.utils.ResourceProvider;
import com.jakewharton.rxrelay2.BehaviorRelay;

import io.reactivex.Observable;

public abstract class BaseLoadingToolbarViewModel extends BaseToolbarViewModel {

    protected BehaviorRelay<Boolean> loadingObservable = BehaviorRelay.createDefault(false);

    public BaseLoadingToolbarViewModel(ResourceProvider resourceProvider, SchedulerProvider schedulerProvider, UiEvents uiEvents) {
        super(resourceProvider, schedulerProvider, uiEvents);
    }

    public Observable<Boolean> observeLoading() {
        return loadingObservable;
    }

    public void showLoading() {
        loadingObservable.accept(true);
    }

    public void hideLoading() {
        loadingObservable.accept(false);
    }
}