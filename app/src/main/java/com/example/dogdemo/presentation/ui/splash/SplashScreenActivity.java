package com.example.dogdemo.presentation.ui.splash;

import androidx.annotation.Nullable;

import android.annotation.SuppressLint;
import android.os.Bundle;

import com.example.dogdemo.R;
import com.example.dogdemo.databinding.ActivitySplashScreenBinding;
import com.example.dogdemo.presentation.base.BaseActivity;
import com.example.dogdemo.presentation.ui.main.MainActivity;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import timber.log.Timber;

public class SplashScreenActivity extends BaseActivity<ActivitySplashScreenBinding, SplashScreenViewModel> {

    @Override
    public int layoutResId() {
        return R.layout.activity_splash_screen;
    }

    @Override
    protected void initViews(ActivitySplashScreenBinding binding) {
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Timber.d("==== CAN PROCEED: onCreate");
    }

    @Override
    public Class<SplashScreenViewModel> viewModelClass() {
        return SplashScreenViewModel.class;
    }

    @SuppressLint("CheckResult")
    @Override
    protected void registerObservables() {
        super.registerObservables();
        getViewModel().observeOpenMain()
                .doOnSubscribe(this::addDisposable)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(event -> {
                    switch (event.getScreenType()) {
                        case HOME:
                            MainActivity.launch(this);
                            break;
                    }
                }, error -> {
                    Timber.e(error);
                });
    }
}